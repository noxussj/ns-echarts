/**
 * 插拔式开关（颜色）
 */
export const handleColor = (param, option) => {
    const { color } = param;

    if (color) {
        option.visualMap.inRange.color = color;
    }
};
