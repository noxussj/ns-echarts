/**
 * 插拔式开关（改变方向）
 */
export const handleDirection = (param, option, axis) => {
    const { direction } = param;

    if (direction === 'y') {
        option.xAxis.type = 'value';
        option.xAxis.data = null;

        option.yAxis.type = 'category';
        option.yAxis.data = axis;
    }
};

/**
 * 插拔式开关（堆叠）
 */
export const handleStack = (param, item) => {
    const { stack } = param;

    if (stack === true) {
        item.stack = 1;
    }
};

/**
 * 插拔式开关（背景色）
 */
export const handleBackground = (param, item) => {
    const { background } = param;

    if (background) {
        item.showBackground = true;
        item.backgroundStyle = item.backgroundStyle ? item.backgroundStyle : {};
        item.backgroundStyle.color = background;
    }
};

/**
 * 插拔式开关（颜色）
 */
export const handleColor = (param, item, index) => {
    const { color } = param;

    if (color) {
        item.itemStyle = item.itemStyle ? item.itemStyle : {};
        item.itemStyle.color = color[index];
    }
};

/**
 * 插拔式开关（柱状图尺寸）
 */
export const handleBarWidth = (param, item) => {
    const { barWidth } = param;

    if (barWidth) {
        item.barWidth = barWidth;
    }
};
