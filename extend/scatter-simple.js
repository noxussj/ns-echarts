/**
 * 插拔式开关（颜色）
 */
export const handleColor = (param, item, index) => {
    const { color } = param;

    if (color) {
        item.itemStyle = item.itemStyle ? item.itemStyle : {};
        item.itemStyle.color = color[index];
    }
};
