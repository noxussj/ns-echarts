/**
 * 插拔式开关（颜色）
 */
export const handleColor = (param, item, index) => {
    const { color } = param;

    if (color) {
        item.lineStyle = item.lineStyle ? item.lineStyle : {};
        item.itemStyle = item.itemStyle ? item.itemStyle : {};
        item.lineStyle.color = color[index];
        item.itemStyle.color = color[index];
    }
};

/**
 * 插拔式开关（区域渐变）
 */
export const handleAreaColor = (param, item, index, $color) => {
    const { isAreaColor } = param;

    const color = param.color || [$color.theme[0], $color.theme[4], $color.theme[8]];

    if (isAreaColor) {
        item.areaStyle = item.areaStyle ? item.areaStyle : {};
        // item.areaStyle.opacity = 0.5;
        item.areaStyle.color = {
            type: 'linear',
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [
                {
                    offset: 0,
                    color: color[index], // 0% 处的颜色
                },
                {
                    offset: 1,
                    color: color[index].replace(/(\d+)(\))/g, `${0}$2`), // 100% 处的颜色
                },
            ],
        };
    }
};

/**
 * 插拔式开关（曲线）
 */
export const handleSmooth = (param, item, index) => {
    const { isSmooth } = param;

    if (isSmooth) {
        item.smooth = true;
    }
};
