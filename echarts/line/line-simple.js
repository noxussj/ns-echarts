import { $echarts } from '../../index.js';
import { $color, $grid } from '../../libs/echarts-style.js';
import * as $switch from '../../extend/line-simple.js';

export default ({ dom, param, opt }) => {
    let { data } = param;

    /**
     * 导出配置项
     */
    let option = {
        color: $color.theme,
        grid: $grid,
        tooltip: {
            trigger: 'axis',
        },
        legend: {
            icon: 'rect',
            top: 10,
            right: 20,
            itemWidth: 10,
            itemHeight: 3,
            itemGap: 15,
            textStyle: {
                color: '#fff',
                fontSize: 12,
            },
        },
        xAxis: {
            type: 'category',
            data: data.xAxis,
            axisTick: {
                show: false,
            },
            axisLabel: {
                color: $color.xAxisLabel,
            },
            axisLine: {
                lineStyle: {
                    color: $color.xAxisLine,
                },
            },
        },
        yAxis: {
            type: 'value',
            axisLabel: {
                color: $color.yAxisLabel,
            },
            splitLine: {
                lineStyle: {
                    color: $color.yAxisLine,
                },
            },
        },
        series: [],
    };

    /**
     * 数据处理
     */
    data.series.map((item) => {
        option.series.push({
            name: item.name,
            data: item.data,
            type: 'line',
            emphasis: {
                focus: 'series',
            },
        });
    });

    /**
     * 插拔式开关（整块可移除，或者移除其中一项，也不会有任何影响）
     */
    option.series = option.series.map((item, index) => {
        $switch.handleColor(param, item, index); // 颜色
        $switch.handleAreaColor(param, item, index, $color); // 区域渐变
        $switch.handleSmooth(param, item, index); // 曲线

        return item;
    });

    /**
     * 渲染
     */
    let extensOption = $echarts.extens(opt, option);

    let echarts = $echarts.render(dom, extensOption);
};
