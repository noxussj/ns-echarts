import { $echarts } from '../../index.js';
import { $color } from '../../libs/echarts-style.js';

export default ({ dom, param, opt }) => {
    let { data } = param;

    /**
     * 数据处理
     */
    let seriesData = data;

    /**
     * 导出配置项
     */
    let option = {
        tooltip: {
            show: true,
            trigger: 'axis',
            axisPointer: {
                type: 'shadow',
            },
            formatter: (param) => {
                return `${param[0].name} : ${param[0].value}`;
            },
        },
        polar: {},
        angleAxis: {
            show: false,
            max: function (value) {
                return (value.max * 4) / 3;
            },
        },
        radiusAxis: {
            type: 'category',
            axisLabel: {
                interval: 0,
                color: '#fff',
                fontSize: 12,
            },
            axisLine: {
                show: false,
            },
            axisTick: {
                show: false,
                alignWithLabel: true,
            },
            splitLine: {
                show: true,
                lineStyle: {
                    color: $color.xAxisLine,
                },
            },
            data: seriesData.map((item) => item.name),
        },
        series: [
            {
                type: 'bar',
                coordinateSystem: 'polar',
                barWidth: 8,
                itemStyle: {
                    color: {
                        type: 'linear',
                        x: 1,
                        y: 0,
                        x2: 0,
                        y2: 0.5,
                        colorStops: [
                            { offset: 0, color: $color.theme[0] },
                            { offset: 0.5, color: $color.theme[1] },
                            { offset: 1, color: $color.theme[2] },
                        ],
                    },
                    barBorderRadius: 10,
                    shadowBlur: 20,
                    shadowColor: $color.theme[0],
                },
                data: seriesData,
            },
            {
                type: 'custom',
                coordinateSystem: 'polar',
                data: seriesData,
                renderItem: (params, api) => {
                    let values = [api.value(0), api.value(1)];
                    let coord = api.coord(values);

                    return {
                        type: 'text',
                        position: [3 * Math.sin(coord[3]), 3 * Math.cos(coord[3])],
                        rotation: coord[3] + Math.PI / 2,
                        origin: [coord[0], coord[1]],
                        style: {
                            text: api.value(1),
                            fill: '#2df',
                            fontSize: 12,
                            textAlign: 'right',
                            textVerticalAlign: 'middle',
                            x: coord[0],
                            y: coord[1],
                        },
                    };
                },
            },
        ],
    };

    /**
     * 渲染
     */
    let extensOption = $echarts.extens(opt, option);

    let echarts = $echarts.render(dom, extensOption);
};
