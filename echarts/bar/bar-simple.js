import { $echarts } from '../../index.js';
import { $color, $grid } from '../../libs/echarts-style.js';
import * as $switch from '../../extend/bar-simple.js';

export default ({ dom, param, opt }) => {
    const { data } = param;

    /**
     * 导出配置项
     */
    const option = {
        color: $color.theme,
        grid: $grid,
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow',
            },
        },
        legend: {
            icon: 'rect',
            top: 10,
            right: 20,
            itemWidth: 10,
            itemHeight: 3,
            itemGap: 15,
            textStyle: {
                color: '#fff',
                fontSize: 12,
            },
        },
        xAxis: {
            type: 'category',
            data: data.axis,
            axisTick: {
                show: false,
            },
            axisLabel: {
                color: $color.xAxisLabel,
            },
            axisLine: {
                lineStyle: {
                    color: $color.xAxisLine,
                },
            },
            splitLine: {
                lineStyle: {
                    color: $color.xAxisLine,
                },
            },
        },
        yAxis: {
            type: 'value',
            axisTick: {
                show: false,
            },
            axisLabel: {
                color: $color.yAxisLabel,
            },
            axisLine: {
                lineStyle: {
                    color: $color.yAxisLine,
                },
            },
            splitLine: {
                lineStyle: {
                    color: $color.yAxisLine,
                },
            },
        },
        series: [],
    };

    /**
     * 数据处理
     */
    data.series.map((item) => {
        option.series.push({
            name: item.name,
            data: item.data,
            type: 'bar',
            barWidth: 15,
            emphasis: {
                focus: 'series',
            },
        });
    });

    /**
     * 插拔式开关（整块可移除，或者移除其中一项，也不会有任何影响）
     */
    $switch.handleDirection(param, option, data.axis); // 改变方向

    option.series = option.series.map((item, index) => {
        $switch.handleStack(param, item); // 堆叠
        $switch.handleBackground(param, item); // 背景色
        $switch.handleColor(param, item, index); // 颜色
        $switch.handleBarWidth(param, item); // 柱状图尺寸

        return item;
    });

    /**
     * 渲染
     */
    const extensOption = $echarts.extens(opt, option);

    const echarts = $echarts.render(dom, extensOption);
};
