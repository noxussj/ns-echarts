import { $echarts } from '../../index.js';
import { $color, $grid } from '../../libs/echarts-style.js';
import * as $switch from '../../extend/heatmap-simple.js';

export default ({ dom, param, opt }) => {
    let { data } = param;

    /**
     * 导出配置项
     */
    let option = {
        tooltip: {
            show: true,
        },
        grid: $grid,
        xAxis: {
            type: 'category',
            data: data.xAxis,
            axisTick: {
                show: false,
            },
            axisLabel: {
                color: $color.xAxisLabel,
            },
            axisLine: {
                lineStyle: {
                    color: $color.xAxisLine,
                },
            },
            splitLine: {
                lineStyle: {
                    color: $color.xAxisLine,
                },
            },
            splitArea: {
                show: true,
                areaStyle: {
                    color: ['rgba(255, 255, 255, 0)', 'rgba(255, 255, 255, 0.08)'],
                },
            },
        },
        yAxis: {
            type: 'category',
            data: data.yAxis,
            axisTick: {
                show: false,
            },
            axisLabel: {
                color: $color.yAxisLabel,
            },
            axisLine: {
                lineStyle: {
                    color: $color.yAxisLine,
                },
            },
            splitLine: {
                lineStyle: {
                    color: $color.yAxisLine,
                },
            },
            splitArea: {
                show: true,
                areaStyle: {
                    color: ['rgba(255, 255, 255, 0.08)', 'rgba(255, 255, 255, 0)'],
                },
            },
        },
        visualMap: {
            min: 0,
            max: 10,
            calculable: true,
            orient: 'horizontal',
            top: 10,
            right: 20,
            inRange: {
                color: ['rgba(0, 255, 612, 1)', 'rgba(168, 39, 255, 1)'],
            },
        },
        series: [
            {
                name: 'Punch Card',
                type: 'heatmap',
                label: {
                    show: true,
                },
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowColor: 'rgba(0, 0, 0, 0.5)',
                    },
                },
                data: [],
            },
        ],
    };

    /**
     * 数据处理
     */
    data.series.map((item) => {
        option.series[0].data.push([item[0], item[1], item[2]]);
    });

    /**
     * 插拔式开关（整块可移除，或者移除其中一项，也不会有任何影响）
     */
    $switch.handleColor(param, option);

    /**
     * 渲染
     */
    let extensOption = $echarts.extens(opt, option);

    let echarts = $echarts.render(dom, extensOption);
};
