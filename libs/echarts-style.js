const color = {
    /**
     * 主题色-深色模式
     */
    theme: [
        'rgba(168, 39, 255, 1)',
        'rgba(130, 39, 255, 1)',
        'rgba(90, 0, 255, 1)',
        'rgba(31, 84, 255, 1)',
        'rgba(46, 41, 255, 1)',
        'rgba(6, 0, 255, 1)',
        'rgba(11, 80, 255, 1)',
        'rgba(0, 168, 255, 1)',
        'rgba(0, 204, 255, 1)',
        'rgba(15, 241, 255, 1)',
        'rgba(0, 255, 213, 1)',
        'rgba(0, 255, 612, 1)',
    ],

    /**
     * X轴轴线颜色
     */
    xAxisLine: 'rgba(142, 245, 254, 0.15)',

    /**
     * X轴标签颜色
     */
    xAxisLabel: 'rgba(255, 255, 255, 1)',

    /**
     * Y轴轴线颜色
     */
    yAxisLine: 'rgba(142, 245, 254, 0.1)',

    /**
     * Y轴标签颜色
     */
    yAxisLabel: 'rgba(255, 255, 255, 1)',
};

const grid = {
    /**
     * 上
     */
    top: 60,

    /**
     * 右
     */
    right: 20,

    /**
     * 下
     */
    bottom: 20,

    /**
     * 左
     */
    left: 20,

    /**
     * grid 区域是否包含坐标轴的刻度标签
     */
    containLabel: true,
};

export const $color = color;

export const $grid = grid;
