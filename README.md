# Introduce

-   基于 Echarts5.0 版本进行二次封装的一款插件，这里可能会有人问为什么不直接使用 Echarts 开发。其实也是可以的，更接近底层更加灵活。但是也会有一些缺点，毕竟原生和封装肯定是各有优缺点的，当你需要更多的扩展功能的时候就没那么容易了

-   参考组件化/element 组件思想实现配置图表样式，可通过传参进行修改或者利用继承直接替换原有样式

-   组件化的思想，调用一个组件，不需要关心其内部是怎么实现的，只管传入的参数和数据即可，也就是说我将图表的实现逻辑代码封装到一个 JS 函数中，调用者只管传入参数和数据。为什么是封装到 JS 函数和不是直接写成一个组件，因为 JS 代码在哪里都可以用，够灵活，如果脱离了 vue 那岂不是不能用了（比如到小程序又要重新写一套组件）

# direction

可快速开发图表交付、采用同一套配置所有图表风格统一、插拔式思想方便扩展功能、初学者零基础可进行图表开发（无需关心 option）、中高级可通过传参/继承方式扩展更多个性化图表、代码精简项目更轻量、核心方向快速开发/易维护/易理解

# Example

**示例** https://echarts.noxussj.top/#/

**GitHub** https://github.com/noxussj/echarts-vite

这里不再将详细介绍如何使用，相信各位的学习能力，可通过上方 GitHub 代码来进行学习

# Installing

Using npm:

```
$ cnpm install noxussj-echarts --save
```

Using yarn:

```
$ yarn add noxussj-echarts
```

# Contact Me

-   noxussj `632922356@qq.com`

-   【持续更新 500+前端面试题】https://github.com/noxussj/Interview-Questions/issues

-   利用 THREE.JS 实现 3D 城市建模（珠海市） https://zhuanlan.zhihu.com/p/356079928
